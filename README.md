# :wave: 

My name is Lucas. My preferred pronouns are He/Him/They.

The purpose of this document is to help us work better together by aligning on what I value and how I work.

## What I do

I work as a Principal Engineer within our Secure and Govern product areas. I spend a lot of time focused on measuring efficacy and how to better integrate security into our platform and customer's products. While primarily an engineer, I do (and often prefer) wearing many hats. I enjoy supporting others, both directly and indirectly.

I am the Lorax. I speak for the syntax trees.

### Current areas of focus

*NOTE:* Loosely ordered. I review this approximately once every two weeks as that seems to match my cadence for shifting attention without a swoop-and-poop.

1. :evergreen_tree: [Dependency Graph Visuals](https://gitlab.com/groups/gitlab-org/-/epics/16815)
1. :chart_with_upwards_trend: [Database Optimization efforts](https://gitlab.com/groups/gitlab-org/-/epics/12114) to improve long-term health of the Sec section
    1. Specifically building additional headroom with the [Decomposition of the Sec Database](https://handbook.gitlab.com/handbook/company/working-groups/secure-govern-database-decomposition/) and [rollout plan](https://gitlab.com/groups/gitlab-org/-/epics/15236)
1. :book: Research and roadmap for long-term vulnerability data stores, systems of record, and searchability.
1. :book: Architectural Coaching and guidance
    1. [Compliance Frameworks technical Review](https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/233) - Ensuring we have a simple, scalable approach to modeling frameworks and their enforcement mechanisms
    1. [Database read model for Security policies](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/security_policies_database_read_model/)
    1. [Future of analyzer configuration technical roadmap and design doc](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/security_analyzer_configuration_profiles/)

## What I value

**I value transparency.** Be open about as many things as possible and public by default.

When we keep things open we stay accountable to both internal and external parties. If my beliefs and reasoning are flawed, we are better able to collectively correct them and get better feedback faster.

Transparency also creates awareness, allowing others to grow and learn. It attracts talented individuals personally and professionally, contributing to a better environment both inside and outside the workplace.

**I value asynchronous workflows.** Realtime does not scale well and does not fit all work modes. In certain cases realtime communication can be preferable but asynchronous better allows us to work independently, efficiently, and perform [deep work](http://calnewport.com/books/deep-work/).

**I value documentation.** Documentation supports transparency, asynchronous work, and communication. We should go to great lengths in order to keep knowledge recorded and outside our heads. We shouldn't have to rely on memory and we should better our abilities, not our memories, which are just lessons for future action.

**I value strong opinions, loosely held.** We should have strong defaults but be willing to change our minds based on evidence. 

**I value minimalism.** Both in my professional and personal capacity I admire the journey to reduce something to only what matters.

## Availability / Communication

I prefer asynchronous to synchronous communication (see above), but always prefer communication to non-communication.

I will always appreciate a well-constructed message over an open-ended request for a "quick chat". We can all work more effectively by forcing ourselves to clearly articulate our thinking and providing that information up-front to those with whom we communicate. Some problems do need to be talked out, but not always the best path forward.

Feel free to reach out to me at any time. I prefer public means of communication where others can learn from the discussion, unless its of strictly personal nature.

I snooze communications when unavailable and try to limit my working hours, but happy to respond as soon as I'm able. If I ping you, unless otherwise stated, please respond at your leisure. Unless it is communicated, it is never an emergency.

### Other communication preferences

These are copied/adjusted from the [Diversity, Inclusion and Belonging team member profile template](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/Team-Member-Profile.md).

- I can communicate well in large groups
- I benefit from work tasks being written or backed up with written communication
- I prefer to record zoom meetings so others can review them later
- I prefer to have information or questions prior to meetings or discussions
- I can handle multiple questions or instructions put to me at one time
- Videos are also my least favorite method of visual communication: they are not searchable or skimmable, they become outdated, and they require of a time commitment for me.